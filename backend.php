<?php
    $conn = new mysqli("localhost","root","","crud_vue");
    if($conn->connect_error){
        die("La Conexion Falló!".$conn->connect_error);
    }
    $result = array('error'=>false);
    $action = '';

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'read'){
        $sql = $conn -> query("SELECT * FROM  libros");
        $libros = array();
        while($row = $sql->fetch_assoc()){
            array_push($libros, $row);
        }
        $result['libros'] = $libros;
    }

    if($action == 'create'){
        $descripcion = $_POST['descripcion'];
        $referencia = $_POST['referencia'];
        $cantidad = $_POST['cantidad'];
        $moneda = $_POST['moneda'];
        $precio = $_POST['precio'];
        $isbn = $_POST['isbn'];
        $sql = $conn -> query("INSERT INTO libros (descripcion, referencia, cantidad, moneda, precio, isbn) VALUES ('$descripcion', '$referencia', '$cantidad', '$moneda','$precio', '$isbn')");
        
        if($sql){
            $result['message']="Libro Añadido Exitosamente";
        }
        else{
            $result['error'] = true;
            $result['message'] = "Ocurrió un Fallo al añadir el libro";
        }
    }    

    if($action == 'update'){
        $descripcion = $_POST['Descripcion'];
        $referencia = $_POST['Referencia'];
        $cantidad = $_POST['Cantidad'];
        $moneda = $_POST['Moneda'];
        $precio = $_POST['Precio'];
        $isbn = $_POST['Isbn'];

        $sql = $conn -> query("UPDATE libros SET descripcion='$descripcion', referencia='$referencia', cantidad='$cantidad', moneda='$moneda', precio='$precio' WHERE isbn='$isbn'");
        
        if($sql){
            $result['message']="Libro Actualizado Exitosamente";
        }
        else{
            $result['error'] = true;
            $result['message'] = "Ocurrió un Fallo al actualizar el libro";
        }
    }

    if($action == 'delete'){
        
        $isbn = $_POST['Isbn'];

        $sql = $conn -> query("DELETE FROM libros WHERE Isbn='$isbn'");
        
        if($sql){
            $result['message']="Libro Eliminado Exitosamente";
        }
        else{
            $result['error'] = true;
            $result['message'] = "Ocurrió un Fallo al eliminar el libro";
        }
    }

    $conn->close();
    echo json_encode($result);
?>
