-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-05-2021 a las 04:41:20
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crud_vue`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE `libros` (
  `Descripcion` text NOT NULL,
  `Referencia` text NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Moneda` varchar(3) NOT NULL,
  `Precio` float NOT NULL,
  `Isbn` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`Descripcion`, `Referencia`, `Cantidad`, `Moneda`, `Precio`, `Isbn`) VALUES
('Hola mundo ', 'asm1', 500, 'COP', 5000, 18283),
('Matemáticas 1. Cálculo diferencial, de Zill, Wright, lbarra, forma parte de una colección elaborada especialmente para atender el currículo de un primer curso de cálculo de las instituciones de educación superior. ', 'AO7123', 289, 'USD', 6, 1266322);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `libros`
--
ALTER TABLE `libros`
  ADD PRIMARY KEY (`Isbn`),
  ADD UNIQUE KEY `Referencia` (`Referencia`) USING HASH;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
