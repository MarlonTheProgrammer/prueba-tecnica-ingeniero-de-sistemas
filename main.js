var app=new Vue({
    el: '#app',
    data:{ 
        errorMsg: "",
        successMsg: "",
        showAddModal: false,
        showEditModal: false,
        showDeleteModal: false,
        Books: [],
        newBook: {descripcion:"", referencia:"", cantidad:"", moneda:"", precio:"", isbn:""},
        currentBook: {},
        errors:[],
        errorsFlag: false
    },
    
    mounted: function(){
        this.getAllBooks();
    },

    methods: {
        getAllBooks(){
            axios.get("http://localhost:50/crud-vue-php/backend.php?action=read").then(function(response){
                if(response.data.error){
                    app.errorMsg = response.data.message;
                }

                else{
                    app.Books = response.data.libros;
                }
            });
        },
        addBook(){
            if(app.validationNewBook()){
                var formData = app.toFormData(app.newBook);
                axios.post("http://localhost:50/crud-vue-php/backend.php?action=create", formData).then(function(response){
                    app.newBook = {descripcion:"",referencia:"", cantidad:"", moneda:"", precio:"", isbn:""};
                    if(response.data.error){
                        app.errorMsg = response.data.message;
                    }

                    else{
                        app.successMsg = response.data.message;
                        app.getAllBooks();
                    }
                });
            }

            else{
                this.errors = response.data.message;
            }
        },

        updateBook(){
            var formData = app.toFormData(app.currentBook);
            axios.post("http://localhost:50/crud-vue-php/backend.php?action=update", formData).then(function(response){
            app.currentBook = {};
                if(response.data.error){
                    app.errorMsg = response.data.message;
                }

                else{
                    app.successMsg = response.data.message;
                    app.getAllBooks();
                }
            });  
        },

        deleteBook(){
            var formData = app.toFormData(app.currentBook);
            axios.post("http://localhost:50/crud-vue-php/backend.php?action=delete", formData).then(function(response){
                app.currentBook = {};
                if(response.data.error){
                    app.errorMsg = response.data.message;
                }

                else{
                    app.successMsg = response.data.message;
                    app.getAllBooks();
                }
            });

        },

        toFormData(obj){
            var fd = new FormData();
            for(var i in obj){
                fd.append(i,obj[i]);
            }
            return fd;
        },

        selectBook(Book){
            app.currentBook = Book;
        },

        clearMsg(){
            app.errorMsg = "";
            app.successMsg = "";
            app.errorsFlag = false
        },

        validationNewBook(){


            if(this.newBook.descripcion && this.newBook.referencia && this.newBook.cantidad && this.newBook.moneda && this.newBook.precio && this.newBook.isbn){
                return true;
            }

            this.errors = [];

            if(!this.newBook.descripcion){
                app.clearMsg();
                this.errors.push('Hace falta una descripcion');
                app.errorsFlag = true
            }

            if(!this.newBook.referencia){
                app.clearMsg();
                this.errors.push('Hace falta la referencia.');
                app.errorsFlag = true
            }else if (!this.validReferencia(this.newBook.referencia)) {
                app.clearMsg();
                this.errors.push('La referencia no es valida');
                app.errorsFlag = true
            }

            if(!this.newBook.cantidad){
                app.clearMsg();
                this.errors.push('Hace falta una cantidad.');
                app.errorsFlag = true
            }

            if(!this.newBook.moneda){
                app.clearMsg();
                this.errors.push('Hace falta el tipo de moneda.');
                app.errorsFlag = true
            } else if (!this.validMoneda(this.newBook.moneda)) {
                app.clearMsg();
                this.errors.push('El tipo de moneda no es valido');
                app.errorsFlag = true
            }

            if(!this.newBook.precio){
                app.clearMsg();
                this.errors.push('Hace falta el precio.');
                app.errorsFlag = true
            }

            if(!this.newBook.isbn){
                app.clearMsg();
                this.errors.push('Hace falta un codigo de identificacion isbn.');
                app.errorsFlag = true
            }
        },

        validMoneda(moneda){
            var re = /^\bUSD\b|\bCOP\b|\busd\b|\bcop\b$/;
            return re.test(moneda);
        },

        validReferencia(referencia){
            var re = /^([A-Z|a-z]*+[0-9]*)$/;
            return re.test(referencia);
        }
    }
});
